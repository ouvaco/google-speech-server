# command line attributes
import argparse

# server
import time
import threading
import zmq
import json

# google
import google
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types

# recording
from recording import *

PARSER = argparse.ArgumentParser(description='Ouva Dialog Server')
PARSER.add_argument('--print_devices', action='store_true', help='print a list of audio devices.')
PARSER.add_argument('--audio_device', type=int, default=0, help='recording device index')
PARSER.add_argument('--audio_rate', type=int, default=48000, help='recording audio rate')
PARSER.add_argument('--tcp_port', type=int, help='port to run speech server on')
PARSER.add_argument('--language_code', default="en-US", help='A BCP-47 language tag supported by Google Speech.')
ARGS = PARSER.parse_args()

APPLICATION_QUIT = False
STREAM_OPEN = False
SOCKET = None
MICSTREAM = None
RECEIVED_DATA = None

CLIENT = speech.SpeechClient()

def run_server(tcp_port):
    global STREAM_OPEN
    global APPLICATION_QUIT
    global SOCKET
    global RECEIVED_DATA

    print('Setting up server.')

    context = zmq.Context()
    SOCKET = context.socket(zmq.PAIR)
    SOCKET.bind("tcp://*:%d" % tcp_port)

    print('Server binded to port %s - waiting for START command.' % tcp_port)

    while not APPLICATION_QUIT:
        try:
            # check for a message, this will not block
            message = SOCKET.recv(flags=zmq.NOBLOCK)
            RECEIVED_DATA = json.loads(message)

            # a message has been received
            print("Message received:" + str(message))

            if RECEIVED_DATA['action'] == 'START':
                print('Start command received.')
                STREAM_OPEN = True
            elif RECEIVED_DATA['action'] == 'STOP':
                print('Stop command received.')
                STREAM_OPEN = False
                MICSTREAM.stop()
            elif RECEIVED_DATA['action'] == 'EXIT':
                print('Exit command received.')
                STREAM_OPEN = False
                APPLICATION_QUIT = True

                if MICSTREAM is not None:
                    MICSTREAM.stop()

                break
        except zmq.Again:
            continue
            # print "."

        time.sleep(0.1)


def main():
    global SOCKET
    global MICSTREAM

    if ARGS.print_devices:
        printRecordingDevices()

    server_thread = threading.Thread(target=run_server, args=[ARGS.tcp_port])
    server_thread.daemon = True
    server_thread.start()

    while not APPLICATION_QUIT:
        if STREAM_OPEN:
            print('Starting MicrophoneStream')

            try:
                with MicrophoneStream(ARGS.audio_rate, int(ARGS.audio_rate / 10), ARGS.audio_device) as MICSTREAM:
                    config = None

                    speech_contexts = None

                    if 'speech_contexts' in RECEIVED_DATA:
                        newarray = RECEIVED_DATA['speech_contexts']

                        for index, value in enumerate(newarray):
                            newarray[index] = value[:99]

                        speech_contexts = [types.SpeechContext(phrases=newarray)]

                    config = types.RecognitionConfig(
                        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
                        sample_rate_hertz=ARGS.audio_rate,
                        language_code=ARGS.language_code,
                        speech_contexts=speech_contexts)

                    streaming_config = types.StreamingRecognitionConfig(
                        config=config,
                        interim_results=True)

                    audio_generator = MICSTREAM.generator()
                    requests = (types.StreamingRecognizeRequest(audio_content=content) for content in audio_generator)
                    responses = CLIENT.streaming_recognize(streaming_config, requests)

                    print('Started listening')

                    i = 0
                    for response in responses:
                        if not response.results:
                            continue

                        transcript = ""
                        is_final = False
                        confidence = 0.0
                        
                        for result in response.results:
                            
                            if not result.alternatives:
                                continue

                            transcript += result.alternatives[0].transcript
                            is_final = result.is_final
                            confidence = result.alternatives[0].confidence

                        
                        resultdata = {
                            'transcript': transcript,
                            'is_final': is_final,
                            'confidence': confidence}

                        print(json.dumps(resultdata))
                        SOCKET.send(json.dumps(resultdata).encode('ascii'))

                        if APPLICATION_QUIT:
                            break
            except google.api_core.exceptions.OutOfRange:
                print('Reached maximum duration.')
        else:
            time.sleep(0.1)

if __name__ == '__main__':
    main()
