## Setup

First you need to follow instructions at https://github.com/GoogleCloudPlatform/python-docs-samples/tree/master/speech/cloud-client

The steps in the link above are recreated below, but they might be outdated.

### Authentication

This sample requires you to have authentication setup. Refer to the [Authentication Getting Started Guide](https://cloud.google.com/docs/authentication/getting-started) for instructions on setting up credentials for applications.

Once you have your credentials from Google console, put the json file in the same folder.

### Install Dependencies

1. Install [pip](https://pip.pypa.io/) and [virtualenv](https://virtualenv.pypa.io/) if you do not already have them. You may want to refer to the [Python Development Environment Setup Guide](https://cloud.google.com/python/setup) for Google Cloud Platform for instructions.

2. Create a virtualenv. Following instructions are for Windows command line.

```Console
> virtualenv env
```

3. Activate virtualenv.

```Console
> .\env\Scripts\activate
```

4. Install prerequisite packages: portaudio

5. Grab requirements file from the link above and install all dependencies:

```Console
> pip install -r requirements.txt
```

6. Deactivate virtualenv

```Console
> deactivate
```

## Running

Following instructions are for Windows.

```Console
> .\env\Scripts\activate
> export GOOGLE_APPLICATION_CREDENTIALS=credentials.json
> python google_transcribe_server.py [TCP-PORT] [listening_language]
```

## Usage

Send JSON commands via PAIR socket messaging protocol.

To start listening through microphone and process the feed:

```JSON
{
	"action": "START",
	"speech_contexts": ["word", "hints"]
}
```

To stop listening:

```JSON
{
	"action": "STOP"
}
```

To exit server:

```JSON
{
	"action": "EXIT"
}
```